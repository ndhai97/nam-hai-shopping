<?php
require_once("Core/Controller.php");

class Cart extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function add_to_cart($info)
    {
        $cart_temp = [];
        $cart_temp['id'] = $info['id'];
        $cart_temp['quantity'] = $info['quantity'];
        if (count($_SESSION['cart']) > 0) {
            $this->merge($info);
        } else {
            array_push($_SESSION['cart'], $info);
        }
        header('Location: ' . $_SERVER["REQUEST_URI"] . '');
    }

    function remove_from_cart($product_id)
    {
        $ids = array_column($_SESSION['cart'], 'id');
        if (in_array($product_id, $ids)) {
            $key = array_search($product_id, $ids);
            print_r($key);
            if ($key !== false) {
                unset($_SESSION['cart'][$key]);
                Sort($_SESSION['cart']);
                header('Location: ' . $_SERVER["HTTP_REFERER"] . '');
            }
        }
    }

    function merge($new_added)
    {
        $ids = array_column($_SESSION['cart'], 'id');
        if (in_array($new_added['id'], $ids)) {
            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                if ($_SESSION['cart'][$i]['id'] == $new_added['id']) {
                    $total_quantity = $_SESSION['cart'][$i]['quantity'] + $new_added['quantity'];
                    if ($total_quantity < 11) {
                        $_SESSION['cart'][$i]['quantity'] = $total_quantity;
                    } else {
                        $_SESSION['cart'][$i]['quantity'] = 10;
                    }
                }
            }
        } else {
            array_push($_SESSION['cart'], $new_added);
        }
    }

    function number_of_item()
    {
        if (isset($_SESSION['cart'])) {
            return count($_SESSION['cart']);
        } else {
            return 0;
        }
    }
}