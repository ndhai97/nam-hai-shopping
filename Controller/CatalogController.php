<?php
require_once("Core/Controller.php");

class CatalogController extends Controller
{
    function get_all_catalog()
    {
        $catalog = new MainCatalog();
        return $catalog->get_catalog();
    }

    function get_main_catalog()
    {
        $catalog = new MainCatalog();
        return $catalog->get_main_catalog();
    }
}
