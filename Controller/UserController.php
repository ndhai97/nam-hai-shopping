<?php
require_once("Core/Controller.php");

class UserController extends Controller
{
    private $data;
    private $user_model;
    private $product;
    private $catalog;
    private $album;
    private $order;
    private $order_details;
    private $cart;

    function __construct()
    {
        parent::__construct();
        $this->order_details = new Receipt_line();
        $this->user_model = new User();
        $this->product = new Product();
        $this->catalog = new CatalogController();
        $this->order = new Receipt();
        $this->album = new Album();
        $this->cart = new Cart();
        $this->data['catalog_list'] = $this->catalog->get_all_catalog();
        $this->data['number_of_item_in_cart'] = $this->cart->number_of_item();
        if (isset($_SESSION["UID"])) {
            $this->data['username'] = $this->user_model->get_full_name($_SESSION["UID"]);
            $this->data['address'] = $this->user_model->get_address($_SESSION["UID"]);
        }
    }

    function home()
    {
        $this->data['new_product'] = $this->product->get_new_product(4);
        $product_images = [];
        for ($i = 0; $i < count($this->data['new_product']); $i++) {
            $temp = [];
            $temp['product_images'] = $this->album->get_product_images($this->data['new_product'][$i]['id']);
            array_push($product_images, $temp);
        }
        $this->data['product_images'] = $product_images;
        $this->load->home($this->data);
    }

    function register()
    {
        if (isset($_SESSION['UID'])) {
            header("Location: /");
        } else {
            $this->load->register($this->data);
        }
    }

    function login()
    {
        if (isset($_SESSION['UID'])) {
            header("Location: /");
        } else {
            $this->load->login($this->data);
        }
    }

    function logout()
    {
        session_destroy();
        header("Location: /");
    }

    function cart()
    {
        $this->data['main_catalog'] = $this->catalog->get_main_catalog();
        $this->data['cart'] = $_SESSION['cart'];
        $product_info = [];
        for ($i = 0; $i < count($_SESSION['cart']); $i++) {
            $temp = [];
            $temp['product_info'] = $this->product->get_product($_SESSION['cart'][$i]['id']);
            $temp['product_images'] = $this->album->get_product_images($_SESSION['cart'][$i]['id']);
            $temp['quantity'] = $_SESSION['cart'][$i]['quantity'];
            array_push($product_info, $temp);
        }
        $this->data['product_info'] = $product_info;
        $this->load->cart($this->data);
    }

    function dashboard()
    {
        $this->data['title']    = "Thống kê doanh thu";
        $this->data['lst_year'] = $this->product->get_availiable_year();
        $year = date('Y');

        $statistics_data['jan'] = $this->product->data_by_month(1, $year);
        $statistics_data['feb'] = $this->product->data_by_month(2, $year);
        $statistics_data['mar'] = $this->product->data_by_month(3, $year);
        $statistics_data['apr'] = $this->product->data_by_month(4, $year);
        $statistics_data['may'] = $this->product->data_by_month(5, $year);
        $statistics_data['jun'] = $this->product->data_by_month(6, $year);
        $statistics_data['jul'] = $this->product->data_by_month(7, $year);
        $statistics_data['aug'] = $this->product->data_by_month(8, $year);
        $statistics_data['sep'] = $this->product->data_by_month(9, $year);
        $statistics_data['oct'] = $this->product->data_by_month(10, $year);
        $statistics_data['nov'] = $this->product->data_by_month(11, $year);
        $statistics_data['dec'] = $this->product->data_by_month(12, $year);

        $this->data['trend']           = $this->product->trend();
        $this->data['statistics_data'] = $statistics_data;
        $this->load->dashboard($this->data);
    }

    function manage_product()
    {
        $this->data['title'] = "Quản lý sản phẩm";
        $this->data['main_catalog'] = $this->catalog->get_main_catalog();
        $this->data['new_product'] = $this->product->get_new_product(3);
        $product_images = [];
        for ($i = 0; $i < count($this->data['new_product']); $i++) {
            $temp = [];
            $temp['product_images'] = $this->album->get_product_images($this->data['new_product'][$i]['id']);
            array_push($product_images, $temp);
        }
        $this->data['product_images'] = $product_images;
        $this->load->manage_product($this->data);
    }

    function manage_order()
    {
        $this->data['title'] = "Quản lý đơn hàng";
        //un processed receipt
        $this->data['history']   = $this->order->un_processed_receipt();
        $total = [];
        foreach ($this->data['history'] as $history) {
            $temp = [];
            $temp['total'] = $this->order_details->get_total($history['id']);
            $temp['username'] = $this->user_model->get_full_name($history['user_id']);
            $temp['phone'] = $this->user_model->get_phone_number($history['user_id']);
            $temp['payment_method'] = $history['payment_method'];
            $temp['receipt_line'] = $this->order_details->get_all($history['id']);
            $receipt_line_extra = [];
            foreach ($temp['receipt_line'] as $item) {
                $temp2 = [];
                $temp2['product_name'] = $this->product->get_name($item['product_id']);
                $temp2['quantity'] = $item['quantity'];
                $temp2['price'] = $item['price'];
                array_push($receipt_line_extra, $temp2);
            }
            $temp['receipt_line_extra'] = $receipt_line_extra;

            array_push($total, $temp);
        }
        $this->data['total'] = $total;
        $this->data['no_receipt'] = $this->order->no_un_processed_receipt();
        //processed receipt
        $processed_receipt = $this->order->processed_receipt();
        $total = [];
        foreach ($processed_receipt as $history) {
            $temp = [];
            $temp['id']              = $history['id'];
            $temp['date']            = $history['date'];
            $temp['ship_address']    = $history['ship_address'];
            $temp['ship_address']    = $history['ship_address'];

            $temp['total'] = $this->order_details->get_total($history['id']); //Tổng tiền đơn hàng
            $temp['username'] = $this->user_model->get_full_name($history['user_id']);
            $temp['phone'] = $this->user_model->get_phone_number($history['user_id']);
            $temp['receipt_line'] = $this->order_details->get_all($history['id']);
            $receipt_line_extra = [];
            foreach ($temp['receipt_line'] as $item) {
                $temp2 = [];
                $temp2['product_name'] = $this->product->get_name($item['product_id']);
                $temp2['quantity'] = $item['quantity'];
                $temp2['price'] = $item['price'];
                array_push($receipt_line_extra, $temp2);
            }
            $temp['receipt_line_extra'] = $receipt_line_extra;

            array_push($total, $temp);
        }
        $this->data['processed_receipt'] = $total;
        $this->load->manage_order($this->data);
    }

    function search_product($search_term)
    {
        $this->data['title'] = 'Kết quả tìm kiếm cho "' . $search_term . '"';
        $this->data['search_term'] = $search_term;

        if (isset($_SESSION['IS_ADMIN'])) {
            $this->data['search_result'] = $this->product->search($search_term);
            $product_images = [];
            for ($i = 0; $i < count($this->data['search_result']); $i++) {
                $temp = [];
                $temp['product_images'] = $this->album->get_product_images($this->data['search_result'][$i]['id']);
                array_push($product_images, $temp);
            }
            $this->data['product_images'] = $product_images;
            $this->load->manage_product($this->data);
        } else {
            $this->data['products'] = $this->product->search($search_term);
            $product_images = [];
            for ($i = 0; $i < count($this->data['products']); $i++) {
                $temp = [];
                $temp['product_images'] = $this->album->get_product_images($this->data['products'][$i]['id']);
                array_push($product_images, $temp);
            }
            $this->data['product_images'] = $product_images;
            $this->load->list_product($this->data);
        }
    }

    function profile()
    {
        $this->data['title']     = "Thông tin tài khoản";
        $this->data['user_info'] = $this->user_model->get_full_info($_SESSION['UID']);
        $this->data['history']   = $this->order->history($_SESSION['UID']);
        $total = [];
        foreach ($this->data['history'] as $history) {
            $temp = [];
            $temp = $this->order_details->get_total($history['id']);
            array_push($total, $temp);
        }
        $this->data['total'] = $total;
        $this->load->profile($this->data);
    }

    function product($product_id)
    {
        $this->data['images'] = $this->album->get_product_images($product_id);
        $this->data['product_info'] = $this->product->get_product($product_id);
        $this->load->product($this->data);
    }

    function success()
    {
        $this->load->success($this->data);
    }

    function err()
    {
        $this->load->error($this->data);
    }

    function order()
    {
        $in_cart = [];
        for ($i = 0; $i < count($_SESSION['cart']); $i++) {
            $temp = [];
            $temp['product_info'] = $this->product->get_product($_SESSION['cart'][$i]['id']);
            array_push($in_cart, $temp);
        }
        $this->data['in_cart'] = $in_cart;
        $this->load->order($this->data);
    }

    function list_new_product()
    {
        $this->data['title'] = "Hàng mới về";
        $this->data['products'] = $this->product->get_new_product(20);
        $product_images = [];
        for ($i = 0; $i < count($this->data['products']); $i++) {
            $temp = [];
            $temp['product_images'] = $this->album->get_product_images($this->data['products'][$i]['id']);
            array_push($product_images, $temp);
        }
        $this->data['product_images'] = $product_images;
        $this->load->list_product($this->data);
    }

    function update_info($new_info)
    {
        $new_info['id'] = $_SESSION['UID'];
        unset($new_info['update_user_info']);
        $this->user_model->update($new_info);
        header('location: ?r=profile');
    }

    function del_cart($product_id)
    {
        $this->cart->remove_from_cart($product_id);
    }

    function update_product($new_info)
    {
        unset($new_info['main-catalog']);
        unset($new_info['sub-catalog']);
        unset($new_info['update_product']);
        $this->product->update($new_info);
        if (count($_FILES['images']['name']) > 0) {
            $length = count($_FILES['images']['name']); //number of images
            $this->album->upload_images($new_info['id']);
            for ($i = 0; $i < $length; $i++) {
                $product_images['name'] = $_FILES['images']['name'][$i];
                $product_images['product_id'] = $new_info['id'];
                if (!empty($product_images['name'])) {
                    $this->album->put($product_images);
                }
            }
        }
        header('location: ?r=manage_product');
    }

    function update_receipt_status($new_status)
    {
        $this->order->update($new_status);
    }

    function get_statistics($year)
    {
        $this->data['title'] = "Doanh thu năm {$year}";
        $this->data['choosed_year'] = $year;
        $this->data['lst_year'] = $this->product->get_availiable_year();

        $statistics_data['jan'] = $this->product->data_by_month(1, $year);
        $statistics_data['feb'] = $this->product->data_by_month(2, $year);
        $statistics_data['mar'] = $this->product->data_by_month(3, $year);
        $statistics_data['apr'] = $this->product->data_by_month(4, $year);
        $statistics_data['may'] = $this->product->data_by_month(5, $year);
        $statistics_data['jun'] = $this->product->data_by_month(6, $year);
        $statistics_data['jul'] = $this->product->data_by_month(7, $year);
        $statistics_data['aug'] = $this->product->data_by_month(8, $year);
        $statistics_data['sep'] = $this->product->data_by_month(9, $year);
        $statistics_data['oct'] = $this->product->data_by_month(10, $year);
        $statistics_data['nov'] = $this->product->data_by_month(11, $year);
        $statistics_data['dec'] = $this->product->data_by_month(12, $year);

        $this->data['trend']           = $this->product->trend();
        $this->data['statistics_data'] = $statistics_data;
        $this->load->dashboard($this->data);
    }

    function do_register($register_info)
    {
        unset($register_info['re-password']); // remove re-password
        unset($register_info['register']);
        if ($this->user_model->put($register_info) != NULL) {
            header("Location: ?r=Login");
        }
    }

    function do_login($username, $password)
    {
        if ($this->user_model->is_validate($username, $password)) {
            $_SESSION["UID"] = $this->user_model->get_uid($username);
            $this->data['username'] = $this->user_model->get_full_name($_SESSION["UID"]);
            if ($this->user_model->isAdmin($_SESSION["UID"])) {
                $_SESSION["IS_ADMIN"] = TRUE;
                header("location: ?r=dashboard");
                $this->dashboard();
            } else {
                header('Location: ' . $_SERVER["HTTP_REFERER"] . '');
            }
        }
    }

    function add_product($new_product)
    {
        unset($new_product['main-catalog']); // remove unuse fields
        unset($new_product['sub-catalog']); // remove unuse fields
        unset($new_product['images']); // remove unuse fields
        unset($new_product['add_product']); // remove unuse fields
        $date = date('Y-m-d H:i:s');
        $new_product['date_add'] = $date;

        $product_id = $this->product->put($new_product);
        if (isset($product_id)) {
            if (count($_FILES['images']['name']) > 0) {
                $length = count($_FILES['images']['name']); //number of images
                $this->album->upload_images($product_id);
                for ($i = 0; $i < $length; $i++) {
                    if (isset($_FILES['images']['name'])) {
                        $product_images['name'] = $_FILES['images']['name'][$i];
                        $product_images['product_id'] = $product_id;
                        if (!empty($product_images['name'])) {
                            $this->album->put($product_images);
                        }
                    }
                }
            }
        }
        header("Location: ?r=manage_product");
    }

    function add_to_cart($product_info)
    {
        $this->cart->add_to_cart($product_info);
    }

    function create_order($data)
    {
        $data['create_at'] = date('Y-m-d H:i:s');
        $data['user_id'] = $_SESSION['UID'];
        if (empty($data['note'])) {
            unset($data['note']);
        }
        $receipt_id = $this->order->put($data);

        if (!empty($receipt_id)) {
            $receipt_line = [];
            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                $temp = [];
                $temp['product_id'] = $_SESSION['cart'][$i]['id'];
                $temp['quantity'] = $_SESSION['cart'][$i]['quantity'];
                $temp['price'] = $this->product->getPrice($_SESSION['cart'][$i]['id']);
                $temp['receipt_id'] = $receipt_id;
                array_push($receipt_line, $temp);
            }

            $count = 0;
            foreach ($receipt_line as $product) {
                if ($this->order_details->put($product)) {
                    $count += 1;
                }
            }
            if ($count == count($receipt_line)) {
                $_SESSION['order_status'] = "good";
                header("Location: ?r=success");
            } else {
                echo "2";
                header("Location: ?r=err");
            }
        } else {
            echo "1";
            header("Location: ?r=err");
        }
    }
}

