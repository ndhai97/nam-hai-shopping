<?php
require_once("View/ViewHandler.php");
spl_autoload_register(function ($class_name) {
    $sources = [
        "Controller/{$class_name}.php",
        "Model/{$class_name}.php"
    ];
    foreach ($sources as $source) {
        if (file_exists($source)) {
            require_once("$source");
        }
    }
});

class Controller
{
    var $load;

    function __construct()
    {
        $this->load = new ViewHandler();
    }

    function load($page, $data = [])
    {
        $this->load->$page($data);
    }
}