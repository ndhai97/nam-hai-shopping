<?php
spl_autoload_register(function ($class_name) {
    if (file_exists($class_name)) {
        require_once($class_name);
    }
});

class DatabaseHandler
{
    var $conn;
    var $result;

    function __construct()
    {
        $conn = mysqli_connect("localhost", "root", "", "namhaishopping");
        mysqli_set_charset($conn, "utf8");
        $this->conn = $conn;
    }

    private function create_update_field($data)
    {
        if (isset($data)) {
            $result = "";
            if (isset($data['id'])) {
                unset($data['id']);
            }
            foreach ($data as $key => $field) {
                $result .= $key . " = '" . $field . "'";
                if (next($data) == true) $result .= ", ";
            }
            return $result;
        }
        return FALSE;
    }

    private function create_insert_key_field($data)
    {
        $result = "";
        if (isset($data['id'])) {
            unset($data['id']);
        }
        foreach ($data as $key => $value) {
            $result .= $key;
            if (next($data) == true) $result .= ", ";
        }
        return $result;
    }

    private function create_insert_data_field($data)
    {
        $result = "";
        foreach ($data as $key => $value) {
            $result .= "'" . $value . "'";
            if (next($data) == true) $result .= ", ";
        }
        return $result;
    }

    function findAll()
    {
        $query = "SELECT * FROM " . get_class($this);
        $this->result = mysqli_query($this->conn, $query);
        return $this->result;
    }

    function find($id)
    {
        $query = "SELECT * FROM " . get_class($this) .
            " WHERE id = " . $id;
        $this->result = mysqli_query($this->conn, $query);
        return $this->result;
    }

    function custom_query($cus_query)
    {
        $query = $cus_query;
        $this->result = mysqli_query($this->conn, $query);
        return $this->result;
    }

    function findWith($condition)
    {
        $query = "SELECT * FROM " . get_class($this) .
            " WHERE " . $condition;
        //echo $query;
        $this->result = mysqli_query($this->conn, $query);
        return $this->result;
    }

    function update($data)
    {
        $fields = $this->create_update_field($data);
        $query = "UPDATE " . get_class($this) . " SET {$fields} WHERE id = '" . $data['id'] . "'";
        //echo $query;
        if (mysqli_query($this->conn, $query)) {
            return TRUE;
        }
        return FALSE;
    }

    function put($data)
    {
        $key_fields = $this->create_insert_key_field($data);
        $data_fields = $this->create_insert_data_field($data);
        $query = "INSERT INTO " . get_class($this) . "(" . $key_fields . ") 
                      VALUES (" . $data_fields . ")";
        //echo $query;
        if (mysqli_query($this->conn, $query)) {
            return mysqli_insert_id($this->conn);
        }
        return FALSE;
    }

    function size()
    {
        $result = mysqli_num_rows($this->result);
        return $result;
    }
}
