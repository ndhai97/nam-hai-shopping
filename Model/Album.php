<?php
require_once("Core/DatabaseHandler.php");

class Album extends DatabaseHandler
{
    function __construct()
    {
        parent::__construct();
    }

    function upload_images($product_id)
    {
        $length = count($_FILES['images']['name']);
        for ($i = 0; $i < $length; $i++) {
            if (!empty($_FILES['images']['name'][$i])) {
                if ($_FILES['images']['type'][$i] == "image/jpeg"
                    || $_FILES['images']['type'][$i] == "image/gif"
                    || $_FILES['images']['type'][$i] == "image/png") {
                    $destination = "asset/" . $product_id . "/";
                    $name = $_FILES['images']['name'][$i];
                    if (is_dir($destination)) {
                        move_uploaded_file($_FILES['images']['tmp_name'][$i],
                            $destination . $name);
                    } else {
                        mkdir($destination);
                        move_uploaded_file($_FILES['images']['tmp_name'][$i],
                            $destination . $name);
                    }
                }
            }
        }
    }

    function get_product_images($product_id)
    {
        $condition = "product_id = {$product_id}";
        $result = $this->findWith($condition);
        $arr = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['name'] = $row['name'];
            array_push($arr, $temp);
        }
        return $arr;
    }
}