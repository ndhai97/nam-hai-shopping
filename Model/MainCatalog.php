<?php
require_once("Core/DatabaseHandler.php");

class MainCatalog extends DatabaseHandler
{
    function __construct()
    {
        parent::__construct();
    }

    function get_catalog()
    {
        $subCatalog = new SubCatalog();
        $result = $this->findALL();
        $data = [];
        $count = 1;
        while ($catalog = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['name'] = $catalog['name'];
            $temp['subcatalog_list'] = $subCatalog->get_subcatalog($catalog['id']);
            $data['dataline ' . $count] = $temp;
            $count++;
        }

        return $data;
    }

    function get_main_catalog()
    {
        $result = $this->findALL();
        $data = [];
        while ($catalog = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['id'] = $catalog['id'];
            $temp['name'] = $catalog['name'];
            array_push($data, $temp);
        }

        return $data;
    }
}