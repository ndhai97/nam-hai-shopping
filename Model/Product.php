<?php
require_once("Core/DatabaseHandler.php");

class Product extends DatabaseHandler
{
    function __construct()
    {
        parent::__construct();
    }

    function get_product($product_id)
    {
        $condition = "id = {$product_id}";
        $result = $this->findWith($condition);
        $arr = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['id'] = $row['id'];
            $temp['name'] = $row['name'];
            $temp['price'] = $row['price'];
            $temp['provider'] = $row['provider'];
            $temp['inventory'] = $row['inventory'];
            $temp['description'] = $row['description'];
            $temp['product_type'] = $row['product_type'];
            $temp['status'] = $row['status'];
            array_push($arr, $temp);
        }
        return $arr;
    }

    function currentStatus($product_id)
    {
        $condition = "id = {$product_id}";
        $result = $this->findWith($condition);
        $row = mysqli_fetch_assoc($result);
        return $row['status'];
    }

    function getPrice($product_id)
    {
        $condition = "id = {$product_id}";
        $result = $this->findWith($condition);
        $row = mysqli_fetch_assoc($result);
        return $row['price'];
    }

    function get_name($product_id)
    {
        $condition = "id = {$product_id}";
        $result = $this->findWith($condition);
        $row = mysqli_fetch_assoc($result);
        return $row['name'];
    }

    function get_inventory($product_id)
    {
        $condition = "id = {$product_id}";
        $result = $this->findWith($condition);
        $row = mysqli_fetch_assoc($result);
        return $row['inventory'];
    }

    function setStatus($new_value)
    {
        $this->update($new_value);
    }

    function toggleStatus($product_id)
    {
        $new_value['id'] = $product_id;
        if ($this->currentStatus($product_id) == 0) {
            $new_value['status'] = "1";
            $this->setStatus($new_value);
        } else {
            $new_value['status'] = "0";
            $this->setStatus($new_value);
        }
    }

    function get_new_product($number)
    {
        $condition = "0=0 ORDER BY id DESC LIMIT {$number}";
        $result = $this->findWith($condition);
        $arr = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['id'] = $row['id'];
            $temp['name'] = $row['name'];
            $temp['price'] = number_format($row['price'], 0, '.', '.');
            $temp['status'] = $row['status'];
            array_push($arr, $temp);
        }
        return $arr;
    }

    function data_by_month($month, $year)
    {
        $cus_query = "SELECT quantity, price FROM Receipt_line, Receipt
                        WHERE Receipt.id = Receipt_line.receipt_id AND
                        Receipt.status = 1 AND
                        MONTH(create_at) = {$month} AND YEAR(create_at) = {$year}";
        $result = $this->custom_query($cus_query);
        $count = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $count += $row['quantity'] * $row['price'];
        }
        return $count;
    }

    function get_availiable_year()
    {
        $cus_query = "SELECT YEAR(create_at) AS 'availiable_year'
                    FROM Receipt
                    GROUP BY YEAR(create_at)";
        $result = $this->custom_query($cus_query);
        $list_year = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['year'] = $row['availiable_year'];
            array_push($list_year, $temp['year']);
        }
        return $list_year;
    }

    function trend()
    {
        $cus_query = "SELECT product_id, SUM(quantity) as quantity, price FROM Receipt_line, Receipt
                        WHERE Receipt.id = Receipt_line.receipt_id AND
                        YEAR(create_at) = ".date('Y')." GROUP BY product_id ORDER BY quantity DESC LIMIT 5";
        $result = $this->custom_query($cus_query);
        $trend = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['name']      = $this->get_name($row['product_id']);
            $temp['inventory'] = $this->get_inventory($row['product_id']);
            array_push($trend, $temp);
        }
        return $trend;
    }

    function search($search_term)
    {
        $condition = "name LIKE '%{$search_term}%'";
        $result = $this->findWith($condition);
        $arr = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['id'] = $row['id'];
            $temp['name'] = $row['name'];
            $temp['price'] = number_format($row['price'], 0, '.', '.');
            $temp['status'] = $row['status'];
            array_push($arr, $temp);
        }
        return $arr;
    }
}
