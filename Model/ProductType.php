<?php
require_once("Core/DatabaseHandler.php");

class ProductType extends DatabaseHandler
{
    function __construct()
    {
        parent::__construct();
    }

    function get_product_type($sub_catalog_id)
    {
        $condition = "subCatalogID = '" . $sub_catalog_id . "'";
        $result = $this->findWith($condition);
        $data = [];
        $count = 1;
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['id'] = $row['id'];
            $temp['name'] = $row['name'];
            $data['data_line ' . $count] = $temp;
            $count++;
        }

        return $data;
    }

    function get_subcatalog_id($product_type_id)
    {
        $condition = "id = '" . $product_type_id . "'";
        $result = $this->findWith($condition);
        $row = mysqli_fetch_assoc($result);
        $data = $row['SubCatalogID'];

        return $data;
    }
}