<?php
require_once("Core/DatabaseHandler.php");

class Receipt extends DatabaseHandler
{
    function __construct()
    {
        parent::__construct();
    }

    function history($user_id = "")
    {
        $condition = "user_id = {$user_id}";
        $result = $this->findWith($condition);
        $data = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['id'] = $row['id'];
            $temp['ship_address'] = $row['ship_address'];
            $temp['total'] = 50000;
            $date = new DateTime($row['create_at']);
            $temp['date'] = $date->format('d-m-Y');
            $temp['status'] = $row['status'];
            $temp['user_id'] = $row['user_id'];
            array_push($data, $temp);
        }
        return $data;
    }

    function un_processed_receipt()
    {
        $condition = "status = 0";
        $result = $this->findWith($condition);
        $data = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['id'] = $row['id'];
            $temp['ship_address'] = $row['ship_address'];
            $temp['payment_method'] = $row['payment_method'];
            $temp['total'] = 50000;
            $date = new DateTime($row['create_at']);
            $temp['date'] = $date->format('d-m-Y');
            $temp['status'] = $row['status'];
            $temp['user_id'] = $row['user_id'];
            array_push($data, $temp);
        }
        return $data;
    }

    function processed_receipt()
    {
        $condition = "status = 1";
        $result = $this->findWith($condition);
        $data = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['id'] = $row['id'];
            $temp['ship_address'] = $row['ship_address'];
            $temp['total'] = 50000;
            $date = new DateTime($row['create_at']);
            $temp['date'] = $date->format('d-m-Y');
            $temp['status'] = $row['status'];
            $temp['user_id'] = $row['user_id'];
            array_push($data, $temp);
        }
        return $data;
    }

    function no_un_processed_receipt()
    {
        $condition = "status = 0";
        $here = $this->findWith($condition);
        $result = mysqli_num_rows($here);
        return $result;
    }
}

