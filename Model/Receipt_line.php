<?php
require_once("Core/DatabaseHandler.php");

class Receipt_line extends DatabaseHandler
{
    function __construct()
    {
        parent::__construct();
    }

    function get_total($receipt_id)
    {
        $condition = "receipt_id = {$receipt_id}";
        $result = $this->findWith($condition);
        $count = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $count += $row['price'] * $row['quantity'];
        }
        return $count;
    }

    function get_all($recipt_id)
    {
        $condition = "receipt_id = {$recipt_id}";
        $result = $this->findWith($condition);
        $data = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['product_id'] = $row['product_id'];
            $temp['quantity'] = $row['quantity'];
            $temp['price'] = $row['price'];
            array_push($data, $temp);
        }
        return $data;
    }
}
