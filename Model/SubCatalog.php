<?php
require_once("Core/DatabaseHandler.php");

class SubCatalog extends DatabaseHandler
{
    function __construct()
    {
        parent::__construct();
    }

    function get_subcatalog($main_catalog_id)
    {
        $product_type = new ProductType();

        $condition = "mainCatalogID = '" . $main_catalog_id . "'";
        $result = $this->findWith($condition);
        $data = [];
        $count = 1;
        while ($sub_catalog = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['name'] = $sub_catalog['name'];
            $temp['product_type_list'] = $product_type->get_product_type($sub_catalog['id']);
            $data['data_line ' . $count] = $temp;
            $count++;
        }

        return $data;
    }

    function get_subcatalog_only($main_catalog_id)
    {
        $condition = "mainCatalogID = '" . $main_catalog_id . "'";
        $result = $this->findWith($condition);
        $data = [];
        while ($catalog = mysqli_fetch_assoc($result)) {
            $temp = [];
            $temp['id'] = $catalog['id'];
            $temp['name'] = $catalog['name'];
            array_push($data, $temp);
        }

        return $data;
    }

    function get_main_catalog_id($sub_catalog_id)
    {
        $condition = "id = '{$sub_catalog_id}'";
        $result = $this->findWith($condition);
        $row = mysqli_fetch_assoc($result);
        return $row['MainCatalogID'];
    }
}