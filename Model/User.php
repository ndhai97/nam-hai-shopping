<?php
require_once("Core/DatabaseHandler.php");

class User extends DatabaseHandler
{
    function __construct()
    {
        parent::__construct();
    }

    function is_validate($username, $password)
    {
        $condition = "username='{$username}' AND password='{$password}'";
        $this->findWith($condition);
        if ($this->size() == 1) {
            return TRUE;
        }
        return FALSE;
    }

    function isAdmin($uid)
    {
        $condition = "id = {$uid} AND level = 1";
        $this->findWith($condition);
        if ($this->size() == 1) {
            return TRUE;
        }
        return FALSE;
    }

    function get_uid($username)
    {
        $condition = "username = '" . $username . "'";
        $result = $this->findWith($condition);
        $row = mysqli_fetch_assoc($result);
        if ($this->size() == 1) {
            return $row["id"];
        }
        return null;
    }

    function get_full_name($uID)
    {
        $condition = "id = '" . $uID . "'";
        $result = $this->findWith($condition);
        $row = mysqli_fetch_assoc($result);
        if ($this->size() == 1) {
            return $row['fullname'];
        }
        return null;
    }

    function get_phone_number($uID)
    {
        $condition = "id = '" . $uID . "'";
        $result = $this->findWith($condition);
        $row = mysqli_fetch_assoc($result);
        if ($this->size() == 1) {
            return $row['phone'];
        }
        return null;
    }

    function get_address($uID)
    {
        $condition = "id = '" . $uID . "'";
        $result = $this->findWith($condition);
        $row = mysqli_fetch_assoc($result);
        if ($this->size() == 1) {
            return $row['address'];
        }
        return null;
    }

    function get_full_info($uID)
    {
        $result = $this->find($uID);
        $row = mysqli_fetch_assoc($result);
        $data = [
            'fullname' => $row['fullname'],
            'phone' => $row['phone'],
            'address' => $row['address'],
            'email' => $row['email']
        ];
        return $data;
    }
}
