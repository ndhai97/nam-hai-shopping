$(document).ready(function () {
    function reset_form() {
        $("#cru-form").find("input[type=text], textarea")
            .val("");
        $("#cru-form").find("select")
            .prop("selectedIndex", 0);
    }
    function toggleInsert() {
        $("#manage-product").toggleClass("d-none");
        $("#insert-product").toggleClass("d-none");
        if( $("#fab-add").html() != "<i class=\"fas fa-times\"></i>" ) {
            $("#fab-add").html("<i class=\"fas fa-times\"></i>");
        } else {
            $("#fab-add").html("<i class=\"fas fa-plus\"></i>");
        }
        reset_form();
    }
    //close sidebar
    $(document).on('click', '#sidebar', function(){
        $("#dashboard_menu").toggleClass("d-none");
        $("#content").toggleClass("col-12");
    });

    //add images
    $(".add-images").click(function () {
        $("<div class=\"form-group d-flex justify-content-between images\">\n" +
            "    <input class=\"form-control-file input-sm\"\n" +
            "           type=\"file\" name=\"images[]\" multiple>\n" +
            "    <span class=\"btn-delete\">\n" +
            "        <i class=\"far fa-trash-alt\"></i>\n" +
            "    </span>\n" +
            "</div>").insertAfter("#list-images .wrapper");
    });

    //remove images
    $(document).on('click', '.btn-delete', function () {
        $(this).closest('.images').remove();
    });

    //load catalog
    $(document).on('change', '#catalog', function () {
        $.get("http://nh-shop.test/subcatalog.php?id=" + $(this).val(), function (response) {
            $("#sub-catalog").html(response);
        });
        $.get("http://nh-shop.test/product-type.php?id=" + $(this).val(), function (response) {
            $("#product-type").html(response);
        });
    });
    $(document).on('change', '#sub-catalog', function () {
        $.get("http://nh-shop.test/product-type.php?id=" + $(this).val(), function (response) {
            $("#product-type").html(response);
        });
    });

    // fab
    $(document).on('click', '#fab', function(){
        toggleInsert();
    });
    $(document).on('click', '#btn-cancel', function(){
        toggleInsert();
    });

    // edit
    $(document).on('click', '.btn-edit', function(){
        let product_id = $(this).closest('span').attr("id");
        $.ajax({
            url: 'http://nh-shop.test/product.php?id=' + product_id,
            dataType: 'json',
            type: 'get',
            cache: false,
            success: function(data) {
                $("#cru-form :input").each(function() {
                    var form_input = $(this);
                    $.each(data[0], function(key, value) {
                        if (key == 'main-catalog') {
                            $('#catalog option[value="'+value+'"]')
                                .prop("selected", true).trigger("change");
                        }
                        if (key == 'sub-catalog') {
                            setTimeout(function() {
                                $('#sub-catalog option[value="'+value+'"]')
                                    .prop("selected", true).trigger("change");
                            }, 500);
                        }
                        if (key == 'product_type') {
                            setTimeout(function() {
                                $('#product-type option[value="'+value+'"]')
                                    .prop("selected", true);
                            }, 1000);
                        }
                        if (form_input.attr('name') === key) {
                            form_input.val(value);
                        }
                    });
                });
            }
        });
        $("#hdr-title").html("Cập nhật thông tin sản phẩm");
        $("#btn").attr('name', 'update_product');
        $("#id").val(product_id);
        $("#btn").val("Cập nhật thông tin");
        toggleInsert();
    });

    //change status
    $(document).on('click', '.btn-status', function(){
        let product_id = $(this).closest('span').attr("id");
        $.ajax({
            url: 'http://nh-shop.test/product.php?a=change_status&id=' + product_id,
            dataType: 'json',
            type: 'get',
            cache: false
        });
        location.reload(true);
    });

    $(document).on('click', '.btn-archive', function(get_product_id){
        var product_id = $(this).closest('span').attr("id");
    });
});
