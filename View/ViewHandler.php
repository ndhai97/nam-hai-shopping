<?php

class ViewHandler
{
    function home($data = [])
    {
        include_once("templates/home.php");
    }

    function dashboard($data = [])
    {
        include_once("templates/statistic.php");
    }

    function manage_product($data = [])
    {
        include_once("templates/manage_product.php");
    }

    function manage_order($data = [])
    {
        include_once("templates/manage_order.php");
    }

    function profile($data = [])
    {
        include_once("templates/profile.php");
    }

    function register($data = [])
    {
        include_once("templates/reg.php");
    }

    function login($data = [])
    {
        include_once("templates/login.php");
    }

    function product($data = [])
    {
        include_once("templates/product.php");
    }

    function cart($data = [])
    {
        include_once("templates/cart.php");
    }

    function order($data = [])
    {
        include_once("templates/order.php");
    }

    function list_product($data = [])
    {
        include_once("templates/list-product.php");
    }

    function error($data = [])
    {
        include_once("templates/error.php");
    }

    function success($data = [])
    {
        include_once("templates/success.php");
    }
}
