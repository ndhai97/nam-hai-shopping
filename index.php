<?php
    require_once("Controller/UserController.php");

    $request = isset($_GET['r'])? $_GET['r'] : "home";
    $id = isset($_GET['id'])? $_GET['id'] : null;

    session_start();
    $user = new UserController();
    if(empty($_SESSION['cart'])) {
        $_SESSION['cart'] = [];
    }

    /**
    *    Put this here to avoid the error bellow
    *    "Cannot modify header information - headers already sent by ..."
    *    due to setcookie() and start_session need to start first
    *    before we render the page
    */
    if(isset($_POST['login'])){
        $user->do_login($_POST['username'], $_POST['password']);
    }
    if(isset($_POST['update_user_info'])){
        $user->update_info($_POST);
    }
    if(isset($_POST['register'])){
        $user->do_register($_POST);
    }
    if(isset($_POST['add_product'])){
        $user->add_product($_POST);
    }
    if(isset($_POST['update_product'])){
        $user->update_product($_POST);
    }
    if(isset($_POST['search'])){
        array_pop($_POST); // remove submit
        $user->search_product($_POST['search_term']);
    }
    if(isset($_POST['order'])){
        array_pop($_POST); // remove submit
        $user->create_order($_POST);
    }
    if(isset($_POST['add_to_cart'])){
        array_pop($_POST); // remove submit
        $user->add_to_cart($_POST);
    }
    if(isset($_POST['confirm-order'])){
        array_pop($_POST); // remove submit
        $user->update_receipt_status($_POST);
    }
    if(isset($_POST['choose-year'])) {
        array_pop($_POST); // remove submit
        $user->get_statistics($_POST['year']);
    }

    if(isset($id)){
        $user->$request($id);
    } else {
        $user->$request();
    }
