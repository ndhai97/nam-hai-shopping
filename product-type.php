<?php
    require_once("Model/ProductType.php");

    $sub_catalog_id = isset($_GET['id'])? $_GET['id'] : null;

    if(isset($sub_catalog_id)) {
        $product_type = new ProductType();
        $data = $product_type->get_product_type($sub_catalog_id);

        if(isset($data)) {
            echo '<option value="">Chọn kiểu sản phẩm</option>';
            foreach ($data as $product_type) {
                echo '<option value="'.$product_type['id'].'">'.$product_type['name'].'</option>';
            }
        } else {
            echo '<option value="">Không có dữ liệu</option>';
        }
    }
    if(empty($sub_catalog_id)){
        echo '<option value="">Không có dữ liệu</option>';
    }
