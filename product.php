<?php
    require_once("Model/Product.php");
    require_once("Model/SubCatalog.php");
    require_once("Model/ProductType.php");

    $product_id = isset($_GET['id'])? $_GET['id'] : null;
    $action = isset($_GET['a'])? $_GET['a'] : null;
    $product = new Product();

    if(isset($product_id)) {
        if(isset($action)) {
            if($action = "change_status") {
                $product->toggleStatus($product_id);
            }
        } else {
            Header("Content-Type: application/json");

            $sub_catalog = new SubCatalog();
            $product_type = new ProductType();
            $data = $product->get_product($product_id);

            if(isset($data)) {
                $data[0]['sub-catalog'] = $product_type->get_subcatalog_id($data[0]['product_type']);
                $data[0]['main-catalog'] = $sub_catalog->get_main_catalog_id($data[0]['sub-catalog']);
                echo json_encode($data);
            } else {
                echo '<option value="">Không có dữ liệu</option>';
            }
        }
    }
    if(empty($product_id)){
        echo '<option value="">Không có dữ liệu</option>';
    }