<?php
    require_once("Model/SubCatalog.php");

    $main_catalog_id = isset($_GET['id'])? $_GET['id'] : null;

    if(isset($main_catalog_id)) {
        $sub_catalog = new SubCatalog();
        $data = $sub_catalog->get_subcatalog_only($main_catalog_id);

        if(isset($data)) {
            echo '<option value="">Chọn danh mục phụ</option>';
            foreach ($data as $sub_catalog) {
                echo '<option value="'.$sub_catalog['id'].'">'.$sub_catalog['name'].'</option>';
            }
        } else {
            echo '<option value="">Không có dữ liệu</option>';
        }
    }
    if(empty($main_catalog_id)){
        echo '<option value="">Không có dữ liệu</option>';
    }
