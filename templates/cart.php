<?php
if (isset($_SESSION['IS_ADMIN'])) {
    header('Location: /');
}
require("particals/head.php");
require("particals/other-menu.php");
?>

<div class="container">
    <? if (!empty($data['product_info'])) { ?>
        <div class="row my-2">
            <b class="mr-1">Giỏ hàng</b> (<?= $data['number_of_item_in_cart'] ?> sản phẩm)
        </div>
        <div class="row">
            <? $total = 0; ?>
            <? foreach ($data['product_info'] as $item) { ?>
                <div id="product"
                     class="d-flex justify-content-around align-items-center my-1 p-2 rounded border w-100">
        <span class="preview" style="max-width: 125px;">
            <img src="asset/<?= $item['product_info'][0]['id'] ?>/<? print_r($item['product_images'][0]['name']) ?>"
                 alt="product preview"
                 style="height: auto; width: 100%">
        </span>
                    <span class="info">
            <a href="?r=product&id=<?= $item['product_info'][0]['id'] ?>">
                <div class="product_name mt-2">
                    <?= $item['product_info'][0]['name'] ?>
                </div>
            </a>
            <div class="product_price mt-2 text-danger font-weight-bold">
                <?= number_format($item['product_info'][0]['price'],
                    0, '.', '.'); ?>₫
            </div>
        </span>
                    <span class="quantity">
            <div id="quantity">
                <input type="text" value="<?= $item['quantity'] ?>" name="quantity" class="input-sm"
                       style="width: 40px">
            </div>
        </span>
                    <a href="?r=del_cart&id=<?= $item['product_info'][0]['id'] ?>">
                        <i class="far fa-trash-alt btn-delete"></i>
                    </a>
                </div>
                <? $total += $item['product_info'][0]['price'] * $item['quantity'] ?>
            <? } ?>
        </div>
        <!-- Total -->
        <div class="row">
            <div class="wrapper d-flex justify-content-between align-items-center w-100
            p-2">
                <div class="total">
                    <span class="title mr-2">Tổng tiền:</span>
                    <span class="text-danger" style="font-size: 1.2em">
                    <?= number_format($total,
                        0, '.', '.'); ?>₫
                </span>
                </div>
                <div class="make-order">
                    <a href="?r=Order" class="btn btn-danger">
                        <span class="mr-1">Tiến hành đặt hàng</span> <i class="fas fa-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
    <? } else { ?>
        <div class="row">
            <div class="text-center mx-auto my-5">
                Chưa có sản phẩm
            </div>
        </div>
    <? } ?>
</div>

<?php
require("particals/foot.php");
?>
