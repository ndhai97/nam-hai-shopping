<?php
require("particals/head.php");
require("particals/other-menu.php");
?>

<div class="container">
    <div class="row" style="height: 50vh">
        <h3 class="w-100 d-flex justify-content-center align-items-center">
            <i class="far fa-frown mr-3" style="font-size: 2em"></i>
            Có lỗi sảy ra, vui lòng thử lại.
        </h3>
    </div>
</div>

<?php
require("particals/foot.php");
?>
