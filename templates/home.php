<?php
require("particals/head.php");
require("particals/home-menu.php");
?>
<div id="content" class="container">
    <div class="item-group my-2">
        <div class="d-flex justify-content-between align-items-center px-2">
            <h3 class="text-capitalize">Hàng mới về</h3>
            <a href="?r=list_new_product">xem thêm <i class="fas fa-angle-double-right"></i></a>
        </div>
        <div class="row">
            <? $count = 0; ?>
            <? foreach ($data['new_product'] as $product) { ?>
                <div class="items col-6 col-md-3 p-1">
                    <a href="?r=product&id=<?= $product['id'] ?>" class="card">
                        <img class="card-img-top" style="height: 250px;"
                             src="asset/<?= $product['id'] ?>/<?= $data['product_images'][$count]['product_images'][0]['name'] ?>"
                             alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title"><?= $product['name'] ?></h5>
                            <p class="card-text text-danger"><?= $product['price'] ?>₫</p>
                        </div>
                    </a>
                </div>
                <? $count += 1;
            } ?>
        </div>
    </div>
</div>
<?php
require("particals/foot.php");
?>
