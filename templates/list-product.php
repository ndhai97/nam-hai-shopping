<?php
require("particals/head.php");
require("particals/other-menu.php");
?>

    <div class="container">
        <div class="row pt-3 pl-2">
            <h4 class="text-capitalize" style="color: #3a9e9e;"><?= $data['title'] ?></h4>
        </div>
        <div class="row">
            <? $count = 0; ?>
            <? foreach ($data['products'] as $product) { ?>
                <div class="items col-6 col-md-3 p-1">
                    <a href="?r=product&id=<?= $product['id'] ?>" class="card" style="text-decoration: none;">
                        <img class="card-img-top" style="height: 250px;"
                             src="asset/<?= $product['id'] ?>/<?= $data['product_images'][$count]['product_images'][0]['name'] ?>"
                             alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title"><?= $product['name'] ?></h5>
                            <p class="card-text text-danger"><?= $product['price'] ?>₫</p>
                        </div>
                    </a>
                </div>
                <? $count += 1;
            } ?>
        </div>
    </div>

<?php
require("particals/foot.php");
?>