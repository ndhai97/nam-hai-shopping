<?php
require("particals/head.php");
require("particals/other-menu.php");
?>

    <div id="login">
        <div class="container my-3">
            <div class="row">
                <div class="col-8" id="benifit">
                    <div class="content">
                        <div class="wrap">
                            <b style="font-size: 1.3em">Đăng nhập ngay để</b>
                            <ul class="ml-3">
                                <li>- Dễ dàng đặt mua hàng hơn</li>
                                <li>- Theo dõi đơn hàng đã mua</li>
                                <li>- Nhận nhiều ưu đã hấp dẫn</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <form action="" method="POST">
                        <div class="form-group">
                            <div class="title font-weight-bold">Đăng nhập</div>
                        </div>
                        <div class="form-group">
                            <input type="text"
                                   name="username"
                                   class="form-control" placeholder="Tên đăng nhập">
                        </div>
                        <div class="form-group">
                            <input type="password"
                                   name="password"
                                   class="form-control" placeholder="Mật khẩu">
                        </div>
                        <div class="form-group d-flex justify-content-center align-items-center">
                            <input type="submit" name="login" class="btn btn-danger mr-2" value="Đăng nhập ngay">
                            <span>Hoặc <a href="register" style="color: #3a9e9e">Đăng ký</a></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
require("particals/foot.php");
?>