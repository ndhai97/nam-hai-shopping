<?php
if (!isset($_SESSION['UID'])) {
    header('Location: /');
}
require_once("templates/particals/admin-head.php");
?>
    <div class="col-2" id="dashboard_menu">
        <div class="px-2 py-3">
            <a href="?r=home">
                <img src="/Resources/images/logo-sm.png" alt="linh kien nam hai">
                <span class="ml-2 text-white text-uppercase font-weight-bold">Linh kiện Nam Hải</span>
            </a>
        </div>
        <ul>
            <li>
                <a href="?r=dashboard" class="text-white title" style="text-decoration: none">
                    <div class="items">
                        <div class="title">
                            <i class="fas fa-chart-line" style="color:#EEE"></i>
                            <span class="ml-2">Thống kê</span>
                        </div>
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </a>
            </li>
            <li>
                <a href="?r=manage_product" class="text-white title" style="text-decoration: none">
                    <div class="items">
                        <div class="title">
                            <i class="fas fa-file-invoice" style="color:#EEE"></i>
                            <span class="ml-2">Quản lý sản phẩm</span>
                        </div>
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </a>
            </li>
            <li class="active_item">
                <a href="?r=manage_order" class="text-white title" style="text-decoration: none">
                    <div class="items">
                        <div class="title">
                            <i class="fas fa-archive" style="color:#EEE"></i>
                            <span class="ml-2">Quản lý đơn hàng</span>
                        </div>
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-10 p-0" id="content">
    <nav class="d-flex justify-content-between shadow-sm align-items-center px-3 py-2"
         style="background: #ddd">
        <span id="sidebar">
            <i class="fas fa-bars mr-2"></i>
            <span>Chào, <a class="text-dark font-weight-bold" href="?r=profile"><?= $data['username'] ?></a></span>
        </span>
        <span class="logout">
            <a href="?r=logout">
                <i class="fas fa-power-off"></i>
                <span>Đăng xuất</span>
            </a>
        </span>
    </nav>

    <div class="mt-3" id="content">
        <div class="container">
            <div class="wrapper mx-auto bg-light rounded p-3" style="width: 70%">
                <nav>
                    <div class="nav nav-tabs" id="nav-new-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-new-receipt"
                           data-toggle="tab" href="#nav-new" role="tab"
                           aria-controls="nav-new" aria-selected="true">Đơn hàng (<?= $data['no_receipt'] ?>)</a>
                        <a class="nav-item nav-link" id="nav-old-tab"
                           data-toggle="tab" href="#nav-old" role="tab"
                           aria-controls="nav-old" aria-selected="false">Đơn hàng đã xử lý</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active"
                         id="nav-new" role="tabpanel" aria-labelledby="nav-new-tab">
                        <? $count = 0; ?>
                        <? foreach ($data['history'] as $history) { ?>
                            <div class="items my-2 px-2 border-bottom">
                                <div class="d-flex justify-content-between align-items-center pb-3">
                                    <div class="receipt_id">
                                        Mã hóa đơn: #<?= $history['id'] ?></div>
                                    <div class="date">Ngày đặt: <?= $history['date'] ?></div>
                                </div>
                                <details>
                                    <summary>Xem chi tiết</summary>
                                    <div style=" background: #EEE;
                                padding: 1em;color:#1a1a1a">
                                        <div class="row px-3 mt-2">
                                            <div class="user_info col-6 p-2"
                                                 style="border: 1px solid #DDD;">
                                                <div class="username mb-1">
                                                    <i class="fas fa-user mr-2"></i>
                                                    <?= $data['total'][$count]['username'] ?>
                                                </div>
                                                <div class="phone">
                                                    <i class="fas fa-phone mr-2"></i>
                                                    <?= $data['total'][$count]['phone'] ?>
                                                </div>
                                            </div>
                                            <div class="dest col-6 p-2"
                                                 style="border: 1px solid #DDD;">
                                                <div class="location">
                                                    <i class="fas fa-map-marked-alt mr-2"></i>
                                                    <?= $history['ship_address'] ?>
                                                </div>
                                                <div class="payment_method">
                                                    <?= if($history['payment_method']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-product">
                                            <? foreach ($data['total'][$count]['receipt_line_extra'] as $product_info) { ?>
                                                <div class="d-flex justify-content-between align-items-center px-3 py-2"
                                                     style="border-bottom: 1px solid #DDD;">
                                                    <div class="product_name"><?= $product_info['product_name'] ?></div>
                                                    <div class="details">
                                                        <span class="quantity mr-5">x<?= $product_info['quantity'] ?></span>
                                                        <span class="price"><?= number_format($product_info['price'],
                                                                0, '.', '.') . " ₫"; ?></span>
                                                    </div>
                                                </div>
                                            <? } ?>
                                        </div>
                                        <div class="text-right"
                                             style="border-bottom: 1px solid #DDD;">
                                            <div class="wrap px-3 py-2"
                                                 style="font-size: 1.2em">
                                                Tổng tiền: <span class="total">
                                                <?= number_format($data['total'][$count]['total'],
                                                    0, '.', '.') . " ₫"; ?>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="wrapper">
                                            <form action="" method="post"
                                                  class="d-flex justify-content-between align-items-center my-2">
                                                <select name="status" class="custom-select custom-select-sm w-25">
                                                    <option value="0">Đã tiếp nhận</option>
                                                    <option value="1">Đã hoàn tất đơn hàng</option>
                                                </select>
                                                <input type="hidden" name="id" value="<?= $history['id'] ?>">
                                                <input type="submit"
                                                       class="btn btn-danger"
                                                       name="confirm-order" value="Xác nhận">
                                            </form>
                                        </div>
                                    </div>
                                </details>
                            </div>
                            <? $count += 1;
                        } ?>
                    </div>
                    <div class="tab-pane fade"
                         id="nav-old" role="tabpanel" aria-labelledby="nav-old-tab">
                        <? $count = 0; ?>
                        <? foreach ($data['processed_receipt'] as $processed_receipt) { ?>
                            <div class="items my-2 px-2 border-bottom">
                                <div class="d-flex justify-content-between align-items-center pb-3">
                                    <div class="receipt_id">
                                        Mã hóa đơn: #<?= $processed_receipt['id'] ?>
                                    </div>
                                    <div class="date">Ngày đặt: <?= $processed_receipt['date'] ?></div>
                                </div>
                                <details>
                                    <summary>Xem chi tiết</summary>
                                    <div style=" background: #EEE;
                                padding: 1em;color:#1a1a1a">
                                        <div class="row px-3 mt-2">
                                            <div class="user_info col-6 p-2"
                                                 style="border: 1px solid #DDD;">
                                                <div class="username mb-1">
                                                    <i class="fas fa-user mr-2"></i>
                                                    <?= $data['total'][$count]['username'] ?>
                                                </div>
                                                <div class="phone">
                                                    <i class="fas fa-phone mr-2"></i>
                                                    <?= $data['total'][$count]['phone'] ?>
                                                </div>
                                            </div>
                                            <div class="dest col-6 p-2"
                                                 style="border: 1px solid #DDD;">
                                                <div class="location">
                                                    <i class="fas fa-map-marked-alt mr-2"></i>
                                                    <?= $processed_receipt['ship_address'] ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-product">
                                            <? foreach ($data['processed_receipt'][$count]['receipt_line_extra'] as $product_info) { ?>
                                                <div class="d-flex justify-content-between align-items-center px-3 py-2"
                                                     style="border-bottom: 1px solid #DDD;">
                                                    <div class="product_name"><?= $product_info['product_name'] ?></div>
                                                    <div class="details">
                                                        <span class="quantity mr-5">x<?= $product_info['quantity'] ?></span>
                                                        <span class="price"><?= number_format($product_info['price'],
                                                                0, '.', '.') . " ₫"; ?></span>
                                                    </div>
                                                </div>
                                            <? } ?>
                                        </div>
                                        <div class="text-right"
                                             style="border-bottom: 1px solid #DDD;">
                                            <div class="wrap px-3 py-2"
                                                 style="font-size: 1.2em">
                                                Tổng tiền: <span class="total">
                                                <?= number_format($data['total'][$count]['total'],
                                                    0, '.', '.') . " ₫"; ?>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </details>
                            </div>
                            <? $count += 1;
                        } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php
require_once("templates/particals/admin-foot.php");
?>