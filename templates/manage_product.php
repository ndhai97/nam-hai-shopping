<?php
if (!isset($_SESSION['UID'])) {
    header('Location: /');
}
require_once("templates/particals/admin-head.php");
?>
<div class="col-2" id="dashboard_menu">
    <div class="px-2 py-3">
        <a href="?r=home">
            <img src="/Resources/images/logo-sm.png" alt="linh kien nam hai">
            <span class="ml-2 text-white text-uppercase font-weight-bold">Linh kiện Nam Hải</span>
        </a>
    </div>
    <ul>
        <li>
            <a href="?r=dashboard" class="text-white title" style="text-decoration: none">
                <div class="items">
                    <div class="title">
                        <i class="fas fa-chart-line" style="color:#EEE"></i>
                        <span class="ml-2">Thống kê</span>
                    </div>
                    <i class="fas fa-chevron-right"></i>
                </div>
            </a>
        </li>
        <li class="active_item">
            <a href="?r=manage_product" class="text-white title" style="text-decoration: none">
                <div class="items">
                    <div class="title">
                        <i class="fas fa-file-invoice" style="color:#EEE"></i>
                        <span class="ml-2">Quản lý sản phẩm</span>
                    </div>
                    <i class="fas fa-chevron-right"></i>
                </div>
            </a>
        </li>
        <li>
            <a href="?r=manage_order" class="text-white title" style="text-decoration: none">
                <div class="items">
                    <div class="title">
                        <i class="fas fa-archive" style="color:#EEE"></i>
                        <span class="ml-2">Quản lý đơn hàng</span>
                    </div>
                    <i class="fas fa-chevron-right"></i>
                </div>
            </a>
        </li>
    </ul>
</div>
<div class="col-10 p-0" id="content">
    <nav class="d-flex justify-content-between shadow-sm align-items-center px-3 py-2"
         style="background: #ddd">
        <span id="sidebar">
            <i class="fas fa-bars mr-2"></i>
            <span>Chào, <a class="text-dark font-weight-bold" href="?r=profile"><?= $data['username'] ?></a></span>
        </span>
        <span class="logout">
            <a href="?r=logout">
                <i class="fas fa-power-off"></i>
                <span>Đăng xuất</span>
            </a>
        </span>
    </nav>

    <div class="mt-3" id="manage-product">
        <div class="container">
            <div class="bg-white p-3 w-75 mx-auto shadow-lg rounded text-dark" id="main">
                <form action="" method="post">
                    <div class="input-group">
                        <input type="text" name="search_term"
                               value="<?= isset($data['search_term']) ? $data['search_term'] : ""; ?>"
                               class="form-control" placeholder="Nhập tên sản phẩm cần tìm" autofocus>
                        <div class="input-group-append">
                            <button type="submit" name="search" value="search-product"
                                    class="btn btn-sm btn-search" style="background: #2c3c47; color: #FFF">
                                <span class="fa fa-searchshow">Tìm kiếm</span>
                            </button>
                        </div>
                    </div>
                </form>

                <?php if (isset($data['new_product'])) { ?>
                    <div class="my-1" id="list-product">
                        <div class="font-weight-bold px-2 py-1">Mới thêm gần đây</div>
                        <? $count = 0; ?>
                        <? foreach ($data['new_product'] as $product) { ?>
                            <div class="d-flex justify-content-between align-items-center p-2 rounded w-100 product"
                                 style="border-bottom: 1px solid #DDD">
                            <span class="row">
                                <span class="mr-3">
                                    <img src="asset/<?= $product['id'] ?>/<?= $data['product_images'][$count]['product_images'][0]['name'] ?>"
                                         alt="product preview"
                                         style="height: auto; max-width: 125px; width: 100%">
                                </span>
                                <span>
                                    <div class="product_name mt-2">
                                        <a href="?r=product&id=<?= $product['id'] ?>">
                                            <?= $product['name'] ?>
                                        </a>
                                        <? if ($product['status'] == 1) { ?>
                                            (<span class="text-danger font-weight-bold">
                                                Đã ngừng kinh doanh mặt hàng này
                                            </span>)
                                        <? } ?>
                                    </div>
                                    <div class="product_price mt-2 text-danger font-weight-bold">
                                        <?= $product['price'] ?>₫
                                    </div>
                                </span>
                            </span>
                                <span id="<?= $product['id'] ?>">
                                <i class="fas fa-edit mr-3 btn-edit"></i>
                                <i class="fas fa-archive btn-archive btn-status"></i>
                            </span>
                            </div>
                            <? $count += 1;
                        } ?>
                    </div>
                <?php } ?>
                <?php if (isset($data['search_result'])) { ?>
                    <? if (count($data['search_result']) == 0) {
                        echo "<div class='text-center my-3'>Không tìm thấy kết quả</div>";
                    } else { ?>
                        <div class="my-1" id="list-product">
                            <div class="font-weight-bold px-2 py-1">Kết quả tìm kiếm</div>
                            <? $count = 0; ?>
                            <? foreach ($data['search_result'] as $product) { ?>
                                <div class="d-flex justify-content-between align-items-center p-2 rounded w-100 product"
                                     style="border-bottom: 1px solid #DDD">
                                <span class="row">
                                    <span class="mr-3">
                                        <img src="asset/<?= $product['id'] ?>/<?= $data['product_images'][$count]['product_images'][0]['name'] ?>"
                                             alt="product preview"
                                             style="height: auto; max-width: 125px; width: 100%">
                                    </span>
                                    <span>
                                        <div class="product_name mt-2">
                                            <?= $product['name'] ?>
                                            <? if ($product['status'] == 1) { ?>
                                                (<span class="text-danger font-weight-bold">
                                                    Đã ngừng kinh doanh mặt hàng này
                                                </span>)
                                            <? } ?>
                                        </div>
                                        <div class="product_price mt-2 text-danger font-weight-bold">
                                            <?= $product['price'] ?>₫
                                        </div>
                                    </span>
                                </span>
                                    <span id="<?= $product['id'] ?>">
                                    <i class="fas fa-edit mr-3 btn-edit"></i>
                                    <i class="fas fa-archive btn-archive btn-status"></i>
                                </span>
                                </div>
                            <? $count += 1;} ?>
                        </div>
                    <? } ?>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="m-3 d-none" id="insert-product">
        <div class="container">
            <div class="row">
                <div class="bg-white p-3 w-75 mx-auto shadow-lg rounded text-dark"
                     style="overflow-y: scroll; height: 89vh;">
                    <h2 class="text-light pl-5 py-2 text-left"
                        id="hdr-title"
                        style="border-radius: 2em 5px;
                        background-image: linear-gradient(to right,#ff9187,#da4d3e);">Phiếu nhập</h2>
                    <form action="" enctype="multipart/form-data" method="post" id="cru-form">
                        <div class="d-flex justify-content-between">
                            <div class="left">
                                <div class="form-group">
                                    <label for="name">Tên sản phẩm</label>
                                    <input class="form-control input-sm" placeholder="vd: Mạch Anduino"
                                           type="text" name="name" id="name">
                                </div>
                                <div class="form-group">
                                    <label for="price">Đơn giá</label>
                                    <input class="form-control input-sm" placeholder="Mệnh giá VND"
                                           type="text" name="price" id="price">
                                </div>
                                <div class="form-group">
                                    <label for="provider">Nhà cung cấp</label>
                                    <input class="form-control input-sm" placeholder="Không bắt buộc"
                                           type="text" name="provider" id="provider">
                                </div>
                                <div class="form-group">
                                    <label for="inventory">Số Lượng sản phẩm</label>
                                    <input class="form-control input-sm" placeholder="Số lượng sản phẩm"
                                           type="text" name="inventory" id="inventory">
                                </div>
                            </div>
                            <div class="right ml-3">
                                <div class="form-group">
                                    <label for="description">Ghi chú</label>
                                    <textarea name="description"
                                              class="form-control input-sm"
                                              placeholder="Trọng lượng:&#10;Kích thước:&#10;..."
                                              style="resize: none;"
                                              id="description" cols="30" rows="8"></textarea>
                                </div>
                            </div>
                        </div>
                        <!--choose catalog-->
                        <div class="form-group">
                            <div class="mb-2">Phân loại sản phẩm</div>
                            <span class="catalog">
                                <select class="form-control-sm" name="main-catalog" id="catalog">
                                    <option value="">Chọn danh mục chính</option>
                                    <?php if (isset($data['main_catalog'])) { ?>
                                        <?php foreach ($data['main_catalog'] as $main_catalog) {
                                            echo '<option value="' . $main_catalog['id'] . '">' . $main_catalog['name'] . '</option>';
                                        } ?>
                                    <?php } else {
                                        echo "<option value=''>Không có dữ liệu</option>";
                                    } ?>
                                </select>
                            </span> /
                            <span class="sub-catalog">
                                <select class="form-control-sm" name="sub-catalog" id="sub-catalog">
                                    <option value="">Chọn danh mục phụ</option>
                                </select>
                            </span> /
                            <span class="product-type">
                                <select class="form-control-sm" name="product_type" id="product-type">
                                    <option value="">Chọn kiểu sản phẩm</option>
                                </select>
                            </span>
                        </div>
                        <!--upload images-->
                        <label>Hình minh họa</label>
                        <div id="list-images" class="w-50">
                            <div class="wrapper">
                                <div class="form-group d-flex justify-content-between images">
                                    <input class="form-control-file input-sm"
                                           type="file" name="images[]" multiple>
                                    <div class="btn-delete">
                                        <i class="far fa-trash-alt"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-primary add-images" style="cursor: pointer">
                            <i class="fas fa-plus"></i>
                            Thêm ảnh
                        </div>
                        <div class="text-center mb-3">
                            <input type="hidden" name="id" id="id">
                            <input type="submit" class="btn btn-danger mr-2" name="add_product" value="Thêm sản phẩm"
                                   id="btn">
                            <input type="button" class="btn btn-secondary" name="cancel" value="Hủy" id="btn-cancel">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<div id="fab">
    <span id="fab-add"><i class="fas fa-plus"></i></span>
</div>
<?php
require_once("templates/particals/admin-foot.php");
?>
