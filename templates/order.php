<?php
if (isset($_SESSION['IS_ADMIN']) || empty($_SESSION['cart'])) {
    header('Location: /');
}
require("particals/head.php");
require("particals/other-menu.php");
?>

<div class="container">
    <div class="row">
        <div class="col-4">
            <div class="p-2">
                <div class="title mb-2">
                    <div class="font-weight-bold">
                        Thông tin thanh toán
                    </div>
                </div>
                <form action="" method="post">
                    <? if (!isset($_SESSION['UID'])) { ?>
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" placeholder="Tên đăng nhập">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="login" class="btn btn-danger w-100" value="Đăng nhập">
                        </div>
                    <? } else { ?>
                        <div class="form-group">
                            <input type="text" name="ship_address" class="form-control" placeholder="Ship tới"
                                   value="<?= $data['address'] ?>">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="7" name="note" placeholder="Ghi chú"></textarea>
                        </div>
                    <? } ?>
            </div>
        </div>
        <div class="col-4">
            <div class="p-2">
                <div class="title mb-2 font-weight-bold">Phương thức thanh toán</div>
                <div class="content">
                    <div class="custom-control custom-radio">
                        <input type="radio" id="tt1" name="payment_method" value="1" class="custom-control-input"
                               checked>
                        <label class="custom-control-label" for="tt1">Chuyển khoản qua ngân hàng</label>
                        <div class="info rounded border">
                            <div class="item border-bottom p-2">
                                <div>Vietcombank: 8480844249011</div>
                                <div>Chủ thẻ: Nguyễn Văn A</div>
                                <div>Chi nhánh: Lào Cai</div>
                            </div>
                            <div class="item p-2">
                                <div>Techcombank: 8049255645567</div>
                                <div>Chủ thẻ: Nguyễn Văn A</div>
                                <div>Chi nhánh: Lào Cai</div>
                            </div>
                        </div>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="tt2" name="payment_method" value="2" class="custom-control-input">
                        <label class="custom-control-label" for="tt2">Thanh toán tại nhà (COD)</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="p-2">
                <span class="title mb-2 font-weight-bold">Đơn hàng</span> (<?= count($_SESSION['cart']) ?> sản phẩm)
                <div class="content">
                    <? $total = 0; ?>
                    <? for ($i = 0; $i < count($_SESSION['cart']); $i++) { ?>
                        <div class="item d-flex justify-content-between p-2" style="border-bottom: 1px solid #DDD;">
                            <div class="left">
                                <span class="product_quantity mr-1 font-weight-bold"
                                      style="font-size: 0.8em"><?= $_SESSION['cart'][$i]['quantity'] ?>x</span>
                                <span class="product_name"><?= $data['in_cart'][$i]['product_info'][0]['name'] ?></span>
                            </div>
                            <div class="right">
                                <span class="product_price"><?= number_format($data['in_cart'][$i]['product_info'][0]['price'], 0, '.', '.'); ?>₫</span>
                                <? $total += $data['in_cart'][$i]['product_info'][0]['price'] * $_SESSION['cart'][$i]['quantity'] ?>
                            </div>
                        </div>
                    <? } ?>
                </div>
                <div class="d-flex justify-content-between p-2 font-weight-bold" style="border-bottom: 1px solid #DDD;">
                    <div class="title">Tổng tiền</div>
                    <div class="total text-danger"><?= number_format($total, 0, '.', '.') ?>₫</div>
                </div>
                <div class="p-2">
                    <? if (!isset($_SESSION['UID'])) { ?>
                        <input type="button" class="btn btn-light w-100" value="Đặt mua" disabled>
                    <? } else { ?>
                        <input type="submit" name="order" class="btn btn-danger w-100" value="Đặt mua">
                    <? } ?>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
require("particals/foot.php");
?>
