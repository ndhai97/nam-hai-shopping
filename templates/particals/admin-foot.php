            </div>
        </div>
    </div>
    <script type="text/javascript" src="/Resources/js/jquery-3.3.1.slim.min.js"></script>
    <script type="text/javascript" src="/Resources/js/popper.min.js"></script>
    <script type="text/javascript" src="/Resources/js/bootstrap.min.js"></script>
    <? switch($_SERVER["REQUEST_URI"]) {
        case "/?r=manage_product":
            echo '<script type="text/javascript" src="/Resources/static/admin-product.js"></script>';
            break;
        case "/?r=manage_order":
            echo '<script type="text/javascript" src="/Resources/static/admin-order.js"></script>';
            break;
        case "/?r=dashboard":
            echo '<script type="text/javascript" src="/Resources/js/chart.js"></script>';
            echo '<script type="text/javascript" src="/Resources/static/admin-statistic.js"></script>';
            break;
    } ?>
    </body>
</html>
