<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=isset($data['title'])? $data['title'] : "Document";?></title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/Resources/css/admin-custom.css">
    <link rel="stylesheet" type="text/css" href="/Resources/fa/css/all.css">
</head>
<body>
<div id="dashboard">
    <div class="container-fluid">
        <div class="row">