    <footer class="foot py-4 bg-white">
        <div class="container">
            <div class="row">
                <section class="col-sm-12 col-md-3">
                    <h6 class="title text-uppercase font-weight-bold">Hỗ trợ khách hàng</h6>
                    <p style="font-size: 0.8em">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </p>    
                </section>
                <section class="col-sm-12 col-md-3">
                    <h6 class="title text-uppercase font-weight-bold">Về Nam Hải</h6>
                    <p style="font-size: 0.8em">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </p>
                </section>
                <section class="col-sm-12 col-md-3">
                    <h6 class="title text-uppercase font-weight-bold">Liên kết ngoài</h6>
                    <p style="font-size: 0.8em">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </p>
                </section>
                <section class="col-sm-12 col-md-3">
                    <h6 class="title text-uppercase font-weight-bold">Kết nối với chúng tôi</h6>
                    <div class="wrapper" style="font-size: 1.5em;">
                        <i class="fab fa-facebook-square" style="color: #4267b2"></i>
                        <i class="fab fa-youtube" style="color: #ff0000"></i>
                        <i class="fab fa-pinterest-square" style="color: #e60023"></i>
                    </div>
                </section>
            </div>
        </div>
    </footer>

    <div id="goTop" class="border rounded">
        <i class="fas fa-arrow-up p-3"></i>
    </div>

    <script type="text/javascript" src="/Resources/js/jquery-3.3.1.slim.min.js"></script>
    <script type="text/javascript" src="/Resources/js/popper.min.js"></script>
    <script type="text/javascript" src="/Resources/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/Resources/slick/slick.min.js"></script>
    <script type="text/javascript" src="/Resources/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Trang chủ -->
    <script>
        $(document).ready(function(){
            $('.slick_here').slick({
                dots: true,
                infinite: true,
                slidesToScroll: 1,
                fade: true,
                cssEase: 'linear',
                responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 1,
                        arrows: false
                    }
                }]
            });
            $('#login').click(function(){
                $("#login_dropdown").toggleClass("show-block");
            });
            $(".mobile-menu-item").click(function(){
                $(this).find(".mobile-menu-item-collapse").toggleClass("show-block");
            });
            $("#open-mobile-menu").click(function(){
                $("#mobile-menu").toggleClass("show-block");
                $(".mobile-menu-item-collapse").removeClass("show-block");
            });
            $("#open-submenu-on-other-page").mouseover(function(){
                $("#sidebar").toggleClass("show-block");
            });
            $("#sidebar").mouseleave(function(){
                $("#sidebar").toggleClass("show-block");
            });
            $("input[name='quantity']").TouchSpin({
                initval: 1,
                min: 1,
                max: 10,
                buttondown_class: 'btn bg-light',
                buttonup_class: 'btn bg-light'
            });
            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('#goTop').addClass("show-block");
                } else {
                    $('#goTop').removeClass("show-block");
                }
            });
            
            $('#goTop').click(function(){
                $('html, body').scrollTop(0);
                return false;
            });
            $('#edit').click(function(){
                $("#order-info").toggleClass("d-none");
                $("#edit").toggleClass("btn-danger");
                $("#edit").toggleClass("btn-secondary");
                if($(this).val() != "Giữ nguyên") {
                    $("#edit").val("Giữ nguyên");
                } else {
                    $("#edit").val("Sửa thông tin");
                }
                $("#update-info").toggleClass("d-none");
            });
        });
    </script>
    <!-- Trang sản phẩm -->
    <script>
        $(document).ready(function(){
            $(document).on('click', '.product-preview', function(){
                $('#main-image').attr('src', $(this).attr('src'));
            });
        });
    </script>
</body>
</html>