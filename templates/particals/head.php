<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=isset($data['title'])? $data['title'] : "Document";?></title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/Resources/css/custom.css">
    <link rel="stylesheet" type="text/css" href="/Resources/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/Resources/slick/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="/Resources/fa/css/all.css">
    <link rel="stylesheet" type="text/css" href="/Resources/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css">
</head>
<body style="color: #333">
    <!-- Headers -->
    <nav class="text-white p-2" style="background: #3a9e9e;">
        <div class="container d-flex justify-content-between align-items-center">
            <div id="open-mobile-menu" class="d-block d-xl-none mr-1">
                <i class="fas fa-bars mr-2"></i> 
            </div>
            <div class="logo align-middle">
                <a href="/">
                    <img src="/Resources/images/logo-lg.png" class="d-none d-lg-block" alt="linh kien nam hai">
                    <img src="/Resources/images/logo-sm.png" class="d-block d-lg-none" alt="linh kien nam hai">
                </a>
            </div>
            <div class="searchbox d-none d-xl-block" style="width: 90%; max-width: 450px;">
                <form action="" method="post">
                <div class="input-group">
                    <input type="text" name="search_term" class="form-control"
                           value ="<?= isset($data['search_term'])? $data['search_term']: ""; ?>"
                           placeholder="Nhập tên sản phẩm cần tìm">
                    <div class="input-group-append">
                        <button type="submit" name="search" class="btn btn-sm" style="background: #EEE;">
                            <span class="fa fa-searchshow">Tìm kiếm</span>
                        </button>
                    </div>
                </div>
                </form>
            </div>

            <div class="d-none d-xl-inline-block">
                <? if(isset($data['username'])) { ?>
                    <div id="login" style="cursor: pointer;">
                        <i class="fas fa-user mr-2"></i>
                        <span class="">Chào, <?= $data['username'] ?></span>
                    </div>
                    <div id="login_dropdown" class="text-dark bg-white p-2 rounded shadow mt-3">
                        <div class="p-1" style="border-bottom: 1px solid #DDD;">
                            <i class="fas fa-user-edit"></i>
                            <span><a href="?r=profile">Thông tin cá nhân</a></span>
                        </div>

                        <? if(isset($_SESSION['IS_ADMIN'])) { ?>
                        <div class="p-1" style="border-bottom: 1px solid #DDD;">
                            <i class="fas fa-cogs"></i>
                            <span><a href="?r=dashboard">Bảng điều khiển</a></span>
                        </div>
                        <? } ?>
                        <div class="p-1 d-flex align-items-center">
                            <i class="fas fa-sign-out-alt mr-2"></i>
                            <span>
                                <a class="text-danger" href="?r=logout">Đăng xuất</a>
                            </span>
                        </div>
                    </div>
                <? } else { ?>
                    <div id="login" style="cursor: pointer;">
                        <i class="fas fa-user mr-2"></i>
                        <span class="">đăng nhập</span>
                    </div>
                    <div id="login_dropdown" class="text-dark bg-white p-2 rounded shadow mt-3">
                        <form id="login_form" action="" method="post">
                            <div class="mb-2">
                                <input type="text" name="username"
                                 class="form-control" placeholder="tên đăng nhập">
                            </div>
                            <div class="mb-2">
                                <input type="password" name="password"
                                 class="form-control" placeholder="mật khẩu">
                            </div>
                            <div class="mb-2">
                                <input type="submit" name="login" 
                                class="form-control btn btn-danger" value="đăng nhập">
                            </div>
                        </form>
                        <div>
                            chưa có tài khoản? <a href="register" style="color: #3a9e9e">đăng ký</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <? if(!isset($_SESSION['IS_ADMIN'])) { ?>
                <a href="?r=Cart" id="open-cart">
                    <div class="border p-2 rounded ml-1 text-white">
                        <i class="fas fa-shopping-cart"></i>
                        <span class="d-none d-md-inline-block">Giỏ hàng</span>
                        <span class="p-1 rounded" style="background: #f0945e">
                            <?= isset($data['number_of_item_in_cart'])?
                                $data['number_of_item_in_cart']: 0; ?>
                        </span>
                    </div>
                </a>
            <? } ?>
        </div>
    </nav>
