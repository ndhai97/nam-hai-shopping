<nav id="submenu" class="text-white p-2"  style="background: #3a9e9e">
    <div class="container">
        <div class="row d-block d-xl-none">
            <div class="searchbox-moblie px-3 w-100">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Nhập tên sản phẩm cần tìm">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-sm" style="background: #EEE;">
                            <span class="fa fa-search d-none d-sm-block">Tìm kiếm</span>
                            <span class="fa fa-search d-block d-sm-none"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-none d-xl-block">
            <span class="col-md-3 items text-uppercase">
                <i class="fas fa-bars mr-2"></i> Danh mục sản phẩm
            </span>
            <span class="faq col-md-9">
                <span class="items">
                    <a href="#">
                        <i class="fas fa-archive mr-1"></i> Cách thức vận chuyển
                    </a>
                </span>
                <span class="items">
                    <a href="#">
                        <i class="fas fa-money-check mr-1"></i> Hình thức thanh toán
                    </a>
                </span>
                <span class="items">
                    <a href="#">
                        <i class="fas fa-book-open mr-1"></i>Chính sách hỗ trợ
                    </a>
                </span>
                <span class="items">
                    <a href="#">
                        <i class="far fa-smile-beam mr-1"></i>Khuyến mãi
                    </a>
                </span>
            </span>
        </div>
    </div>
</nav>

<section id="mobile-menu" class="bg-light w-100">
    <ul class="container">
        <li>
            <div class="mobile-menu-item login py-1 px-3">
                <i class="fas fa-user-circle"></i> <a href="#">Đăng nhập</a> | <a href="#">Đăng ký</a>
            </div>
        </li>
        <li>
            <div class="mobile-menu-item py-1 px-3">
                <span class="d-flex justify-content-between">
                    <b>Vi điều khiển nhúng</b><i class="fas fa-angle-down"></i>
                </span>
                <ul class="mobile-menu-item-collapse">
                    <li><a href="#">Test 1</a></li>
                    <li><a href="#">Test 2</a></li>
                    <li><a href="#">Test 3</a></li>
                </ul>
            </div>
        </li>
        <li>
            <div class="mobile-menu-item py-1 px-3">
                <span class="d-flex justify-content-between">
                    <b>Cảm biến</b><i class="fas fa-angle-down"></i>
                </span>
            </div>
        </li>
        <li>
            <div class="mobile-menu-item py-1 px-3">
                <span class="d-flex justify-content-between">
                    <b>Linh kiện điện tử</b><i class="fas fa-angle-down"></i>
                </span>
            </div>
        </li>
        <li>
            <div class="mobile-menu-item py-1 px-3">
                <span class="d-flex justify-content-between">
                    <b>Dụng cụ - Phụ kiện</b><i class="fas fa-angle-down"></i>
                </span>
            </div>
        </li>
        <li>
            <div class="mobile-menu-item py-1 px-3">
                <span class="d-flex justify-content-between">
                    <b>Pin/Nguồn</b><i class="fas fa-angle-down"></i>
                </span>
            </div>
        </li>
    </ul>
</section>

<section id="subsubmenu" class="container p-1">
    <div class="row">
    <?php if(isset($data['catalog_list'])) { ?>
        <ul id="sidebar" class="col-md-3 d-none d-xl-block">
        <?php foreach ($data['catalog_list'] as $catalog) { ?>
            <li class="items border p-2 mb-1">
                <?= $catalog['name'] ?> <i class="fas fa-chevron-right"></i>
                <ul class="sidebar_expand w-75 h-100 shadow rounded bg-light p-2">
                    <div class="row px-2">
                        <?php foreach ($catalog['subcatalog_list'] as $subCatalog) { ?>   
                            <ul class="col-4">
                                <span class="font-weight-bold">
                                   <?= $subCatalog["name"] ?>
                                </span>
                                <?php foreach ($subCatalog['product_type_list'] as $product_type) { ?>
                                    <li><a href="#"><?= $product_type['name'] ?></a></li>
                                <? } ?>
                            </ul>
                        <?php } ?>
                    </div>
                </ul>
            </li>
        <?php } ?>
        </ul>
    <?php } ?>
        <div id="slide" class="col-xl-9 col-lg-12">
            <div class="slick_here p-2">
                <div>
                    <img src="Resources/images/banner1.png" alt="banner1">
                </div>
                <div>
                    <img src="Resources/images/banner2.png" alt="banner1">
                </div>
            </div>
        </div>
    </div>
</section>