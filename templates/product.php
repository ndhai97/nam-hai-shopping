<?php
require("particals/head.php");
require("particals/other-menu.php");
?>

    <div class="container">
        <? if (isset($data['product_info'][0])) { ?>
            <div class="row">
                <div class="col-4 p-5">
                    <? foreach ($data['images'] as $image) {
                        if ($image['name'] == reset($data['images'][0])) { ?>
                            <img src="asset/<?= $data['product_info'][0]['id'] ?>/<?= $image['name'] ?>"
                                 alt="preview" class="w-100" id="main-image">
                        <? } ?>
                        <img src="asset/<?= $data['product_info'][0]['id'] ?>/<?= $image['name'] ?>"
                             alt="preview" class="product-preview" style="width: 30%;">
                    <? } ?>
                </div>
                <div class="col-8 p-2">
                    <div class="product_name mt-2" style="font-size: 2em">
                        <?= $data['product_info'][0]['name'] ?>
                    </div>
                    <div class="product_price mt-2 text-danger font-weight-bold">
                        <? if ($data['product_info'][0]['status'] == 1) {
                            echo "Ngừng kinh doanh";
                        } else if ($data['product_info'][0]['inventory'] < 1) {
                            echo "Sản phẩm đã hết hàng";
                        } else {
                            echo number_format($data['product_info'][0]['price'],
                                    0, '.', '.') . "₫";
                        } ?>
                    </div>
                    <? if ($data['product_info'][0]['status'] == 0 || $data['product_info'][0]['inventory'] < 1) { ?>
                        <? if (!isset($_SESSION['IS_ADMIN'])) { ?>
                            <div class="add_to_cart mt-3">
                                <form action="" method="post">
                                    <div id="quantity" class="d-inline-block">
                                        <input type="hidden" value="<?= $data['product_info'][0]['id'] ?>" name="id">
                                        <input type="text" value="" name="quantity" class="input-sm"
                                               style="width: 40px">
                                    </div>
                                    <div class="ml-3 d-inline-block">
                                        <input type="submit" name="add_to_cart" value="Thêm vào giỏ hàng"
                                               class="btn btn-danger">
                                    </div>
                                </form>
                            </div>
                        <? } ?>
                        <div class="provider mt-3">
                            <span class="title font-weight-bold">Nhà cung cấp: </span>
                            <span class="provider">
                    <?= isset($data['product_info'][0]['provider']) ?
                        $data['product_info'][0]['provider'] : 'Chưa cập nhật' ?>
                </span>
                        </div>
                        <div class="description mt-3">
                            <div class="title font-weight-bold">Thông tin chi tiết:</div>
                            <div class="content mt-2">
                                <?= isset($data['product_info'][0]['description']) ?
                                    nl2br($data['product_info'][0]['description']) : "Chưa cập nhật" ?>
                            </div>
                        </div>
                    <? } ?>
                </div>
            </div>
        <? } else { ?>
            <div class="row mx-auto">
                <div class="text-center py-5 mx-auto">
                    Sản phẩm không tồn tại
                </div>
            </div>
        <? } ?>
    </div>

<?php
require("particals/foot.php");
?>