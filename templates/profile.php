<?php
require("particals/head.php");
require("particals/other-menu.php");
?>
<div id="profile" xmlns="http://www.w3.org/1999/html">
    <div class="container py-3">
        <div class="row">
            <div class="col-8 <?= isset($_SESSION['IS_ADMIN']) ? 'd-none' : null ?>" id="order-info">
                <table class="table table-hover">
                    <thead class="text-white" style="background: rgba(63,166,166,0.9)">
                    <tr>
                        <th scope="col">Mã hóa đơn</th>
                        <th scope="col">Ngày đặt</th>
                        <th scope="col">Tổng tiền</th>
                        <th scope="col">Tình trạng</th>
                    </tr>
                    </thead>
                    <tbody style="cursor: pointer">
                    <? $count = 0; ?>
                    <? foreach ($data['history'] as $history) { ?>
                    <tr>
                        <th scope="row">#<?= $history['id'] ?></th>
                        <td><?= $history['date'] ?></td>
                        <td><?= number_format($data['total'][$count],
                                    0, '.', '.') . " ₫"; ?></td>
                        <td><?= isset($history['status'])? "Đã tiếp nhận đơn hàng" : "Đã giao hàng"; ?></td>
                    </tr>
                    <? $count+=1;} ?>
                    </tbody>
                </table>
            </div>
            <div class="col-8 <?= isset($_SESSION['IS_ADMIN']) ? null : "d-none" ?> my-auto" id="update-info">
                <form action="" method="post">
                    <div class="row">
                        <div class="form-group col-6">
                            <label for="fullname">Tên đầy đủ</label>
                            <input type="text" name="fullname"
                                   value="<?= $data['user_info']['fullname'] ?>"
                                   id="fullname" class="form-control"/>
                        </div>
                        <div class="form-group col-6">
                            <label for="phone">Số điện thoại</label>
                            <input type="text" name="phone"
                                   value="<?= $data['user_info']['phone'] ?>"
                                   id="phone" class="form-control"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label for="address">Địa chỉ</label>
                            <input type="text" name="address"
                                   value="<?= $data['user_info']['address'] ?>"
                                   id="address" class="form-control"/>
                        </div>
                        <div class="form-group col-6">
                            <label for="email">Email</label>
                            <input type="text" name="email"
                                   value="<?= $data['user_info']['email'] ?>"
                                   id="email" class="form-control"/>
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" name="update_user_info"
                               class="btn btn-danger w-100 mx-3" value="Cập nhật thông tin"/>
                    </div>
                </form>
            </div>
            <?php if (isset($data['user_info'])) { ?>
                <div class="col-4">
                    <div class="w-100 p-4 border rounded">
                        <div class="info">
                            <span class="font-weight-bold">Họ và tên:</span>
                            <span><?= $data['user_info']['fullname'] ?></span>
                        </div>
                        <div class="info">
                            <span class="font-weight-bold">Số điện thoại:</span>
                            <span><?= $data['user_info']['phone'] ?></span>
                        </div>
                        <div class="info">
                            <span class="font-weight-bold">Địa chỉ:</span>
                            <span><?= $data['user_info']['address'] ?></span>
                        </div>
                        <div class="info">
                            <span class="font-weight-bold">Email:</span>
                            <span><?= $data['user_info']['email'] ?></span>
                        </div>
                        <? if (!isset($_SESSION['IS_ADMIN'])) { ?>
                            <div class="action text-center mt-2">
                                <input type="button" class="btn btn-danger w-100" id="edit" value="Sửa thông tin"/>
                            </div>
                        <? } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php
require("particals/foot.php");
?>
