<?php
require("particals/head.php");
require("particals/other-menu.php");
?>

    <div id="regsister">
        <div class="container my-3">
            <div class="row">
                <div class="col-8" id="benifit">
                    <div class="content">
                        <div class="wrap">
                            <b style="font-size: 1.3em">Đăng ký ngay để</b>
                            <ul class="ml-3">
                                <li>- Dễ dàng đặt mua hàng hơn</li>
                                <li>- Theo dõi đơn hàng đã mua</li>
                                <li>- Nhận nhiều ưu đã hấp dẫn</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <form action="" method="post">
                        <div class="form-group">
                            <div class="title font-weight-bold">Tạo tài khoản</div>
                        </div>
                        <div class="form-group">
                            <input type="text"
                                   name="fullname"
                                   class="form-control" placeholder="Tên đăng nhập"/>
                        </div>
                        <div class="form-group d-flex justify-content-between">
                            <div class="left w-50 pr-2">
                                <input type="password"
                                       name="password"
                                       class="form-control" placeholder="Mật khẩu"/>
                            </div>
                            <div class="right w-50 pl-2">
                                <input type="password"
                                       name="re-password"
                                       class="form-control" placeholder="Nhập lại mật khẩu"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text"
                                   name="username"
                                   class="form-control" placeholder="Họ và tên"/>
                        </div>
                        <div class="form-group">
                            <input type="text"
                                   name="phone"
                                   class="form-control" placeholder="Số điện thoại"/>
                        </div>
                        <div class="form-group">
                            <input type="text"
                                   name="email"
                                   class="form-control" placeholder="Địa chỉ email"/>
                        </div>
                        <div class="form-group">
                            <input type="text"
                                   name="address"
                                   class="form-control" placeholder="Địa chỉ"/>
                        </div>
                        <div class="form-group d-flex justify-content-center align-items-center">
                            <input type="submit"
                                   name="register"
                                   class="btn btn-danger mr-2" value="Đăng ký ngay">
                            <span>Hoặc <a href="Login" style="color: #3a9e9e">Đăng nhập</a></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
require("particals/foot.php");
?>