<?php
if (!isset($_SESSION['UID'])) {
    header('Location: /');
}
require_once("templates/particals/admin-head.php");
?>
<div class="col-2" id="dashboard_menu">
    <div class="px-2 py-3">
        <a href="?r=home">
            <img src="/Resources/images/logo-sm.png" alt="linh kien nam hai">
            <span class="ml-2 text-white text-uppercase font-weight-bold">Linh kiện Nam Hải</span>
        </a>
    </div>
    <ul>
        <li class="active_item">
            <a href="?r=dashboard" class="text-white title" style="text-decoration: none">
                <div class="items">
                    <div class="title">
                        <i class="fas fa-chart-line" style="color:#EEE"></i>
                        <span class="ml-2">Thống kê</span>
                    </div>
                    <i class="fas fa-chevron-right"></i>
                </div>
            </a>
        </li>
        <li>
            <a href="?r=manage_product" class="text-white title" style="text-decoration: none">
                <div class="items">
                    <div class="title">
                        <i class="fas fa-file-invoice" style="color:#EEE"></i>
                        <span class="ml-2">Quản lý sản phẩm</span>
                    </div>
                    <i class="fas fa-chevron-right"></i>
                </div>
            </a>
        </li>
        <li>
            <a href="?r=manage_order" class="text-white title" style="text-decoration: none">
                <div class="items">
                    <div class="title">
                        <i class="fas fa-archive" style="color:#EEE"></i>
                        <span class="ml-2">Quản lý đơn hàng</span>
                    </div>
                    <i class="fas fa-chevron-right"></i>
                </div>
            </a>
        </li>
    </ul>
</div>
<div class="col-10 p-0" id="content">
    <nav class="d-flex justify-content-between shadow-sm align-items-center px-3 py-2"
         style="background: #ddd">
        <span id="sidebar">
            <i class="fas fa-bars mr-2"></i>
            <span>Chào, <a class="text-dark font-weight-bold" href="?r=profile"><?= $data['username'] ?></a></span>
        </span>
        <span class="logout">
            <a href="?r=logout">
                <i class="fas fa-power-off"></i>
                <span>Đăng xuất</span>
            </a>
        </span>
    </nav>

    <div class="mt-3" id="content">
        <div class="container">
            <div class="row mx-auto rounded">
                <div class="col-9">
                    <div class="option">
                        <form action="" method="post" class="form-inline">
                            <select class="custom-select mr-3" name="year">
                                <? foreach ($data['lst_year'] as $year) { ?>
                                    <? if ($data['choosed_year'] == $year) { ?>
                                        <option value="<?= $year ?>" selected><?= $year ?></option>
                                    <? } else { ?>
                                        <option value="<?= $year ?>"><?= $year ?></option>
                                    <? }
                                } ?>
                            </select>
                            <input type="submit"
                                   class="btn btn-primary"
                                   name="choose-year" value="Xem">
                        </form>
                    </div>
                    <div class="chart mt-3">
                        <canvas id="month-chart"></canvas>
                    </div>
                </div>
                <div class="col-3">
                    <h4>Đang bán chạy</h4>
                    <ol>
                        <? foreach ($data['trend'] as $trend_product) { ?>
                        <li>
                            <span><?=$trend_product['name']?></span>
                            <span>(Tồn kho: <?=$trend_product['inventory']?>)</span>
                        </li>
                        <? } ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <? $statistics_data = array_values((array)$data['statistics_data']); ?>
</div>
<?php
require_once("templates/particals/admin-foot.php");
?>
<script>
    var ctx = document.getElementById('month-chart');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['tháng 1', 'tháng 2', 'tháng 3', 'tháng 4', 'tháng 5', 'tháng 6', 'tháng 7', 'tháng 8', 'tháng 9', 'tháng 10', 'tháng 11', 'tháng 12'],
            datasets: [{
                data: <?= json_encode($statistics_data); ?>,
                borderColor: "#36a2eb",
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Doanh thu theo tháng (Đơn vị: VND)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            }
        }
    });
</script>
