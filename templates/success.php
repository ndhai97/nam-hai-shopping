<?php
header("Refresh: 3; url=/");
require("particals/head.php");
require("particals/other-menu.php");
if ($_SESSION['order_status'] == "good") {
    unset($_SESSION['cart']);
    unset($_SESSION['order_status']);
}
?>

<div class="container">
    <div class="row" style="height: 50vh">
        <h3 class="w-100 d-flex justify-content-center align-items-center">
            <i class="far fa-smile mr-3" style="font-size: 2em"></i>
            Giao dịch thành công, chúc bạn mua sắm vui vẻ
        </h3>
    </div>
</div>

<?php
require("particals/foot.php");
?>
